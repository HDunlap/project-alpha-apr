from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProjectForm

# Create your views here.


@login_required(login_url="login")
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {"project": project}
    return render(request, "projects/list.html", context)


@login_required(login_url="login")
def show_project(request, id):
    project = Project.objects.get(owner=request.user, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required(login_url="login")
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
