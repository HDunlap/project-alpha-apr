from django.shortcuts import render, redirect
from .forms import CreateTaskForm
from django.contrib.auth.decorators import login_required
from .models import Task

# Create your views here.


@login_required(login_url="login")
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required(login_url="login")
def list_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {"task": task}
    return render(request, "tasks/list.html", context)
